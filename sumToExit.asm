#include "p16F877.inc"

		;Initialization
		element equ 20h
		sum 	equ 21h
		start	equ d'1'
		max		equ d'10'
		org		00
		bsf STATUS,RP0
		movlw b'00000000'
		movwf TRISB	
		bcf STATUS,RP0

		;Initialiaze memory
		movlw 00
		movwf sum
		movlw start
		movwf element
		

		
main 	movf element,w
		addwf sum,1
		movf element,w
		sublw max
		btfsc STATUS,Z
		goto $+3
		incf element
		goto main
		movf sum,w
		movwf PORTB
		end